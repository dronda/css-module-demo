import { testClass, header } from './style.css'

const Test = () => (
    <div class={testClass}>
        <h1 class={header}>Hello World</h1>
    </div>
)

export default Test
