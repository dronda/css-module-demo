import { FunctionalComponent } from 'preact'
import Test from './Test'
import Test2 from './Test2'

const Root = (): FunctionalComponent => {
    return (
        <div>
            <Test />
            <Test2 />
        </div>
    )
}

export default Root
